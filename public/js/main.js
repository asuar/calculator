//TODO: refactor try catch into method

let operands = [],
    operators = [];
let displayString = "";
let currentNumber = "";
let currentDisplay = "";

function init() {
    clearDisplay();
    createNumberButtons();
    createOperatorButtons();
    window.addEventListener('keydown', handleKeyboardPress);
}

function addNumbers(x, y) {
    return x + y;
}

function subtractNumbers(x, y) {
    return y - x;
}

function multiplyNumbers(x, y) {
    return x * y;
}

function divideNumbers(x, y) {
    if (x !== 0)
        return (y / x).toFixed(3);
    else
        return "Cannot divide by zero! Can you?";
}

function operate(operator, x, y) {
    let sum;

    if (!isNaN(x))
        x = parseFloat(x);

    if (!isNaN(y))
        y = parseFloat(y);

    switch (operator) {
        case "+":
            sum = addNumbers(x, y);
            break;
        case "-":
            sum = subtractNumbers(x, y);
            break;
        case "\xD7":
            sum = multiplyNumbers(x, y);
            break;
        case "\xF7":
            sum = divideNumbers(x, y);
            break;
        default:
            sum = undefined;
    }
    return sum;
}

function updateDisplay(value) {
    let content = document.getElementById("calculator-screen-current").textContent;

    if(value == ""){
        clearDisplay();
        return;
    }

    if (content === '0' || content === '') {
        displayString = value;
        currentDisplay = value;
        document.getElementById("calculator-screen-previous").textContent = value;
        setCurrentDisplay(currentDisplay);
    } else {
        document.getElementById("calculator-screen-previous").textContent += value;
        displayString += value;

        if (value.match(/^[0-9]+$/) != null && currentDisplay != "") {
            currentDisplay += value;
            setCurrentDisplay(currentDisplay);
        } else if (value.match(/^[0-9]+$/) != null && currentDisplay == "") {
            currentDisplay = value;
            setCurrentDisplay(currentDisplay);
        } else if (value.match(/^[.]$/) != null) {
            currentDisplay += value;
            setCurrentDisplay(currentDisplay);
        } else {
            currentDisplay = "";
        }
    }
}

function clearDisplay() {
    displayString = '0';
    currentNumber = "0";
    currentDisplay = "0";
    document.getElementById("calculator-screen-previous").textContent = '';
    setCurrentDisplay(currentDisplay) ;
}

function createNumberButtons() {
    const numberButtons = document.querySelectorAll('.button-number');
    numberButtons.forEach((button) => button.addEventListener('click', pressNumberButton));
}

function pressNumberButton(e) {
    let target;
    try {
        target = e.target.textContent;
    } catch (err) {
        target = e.textContent;
    }

    updateDisplay(target);
    if (operands.length == operators.length) {
        currentNumber = target;
        operands.push(currentNumber);
    } else {
        currentNumber += target;
        operands.pop();
        operands.push(currentNumber);
    }
}

function createOperatorButtons() {
    const operatorButtons = document.querySelectorAll('.button-operator');
    operatorButtons.forEach((button) => button.addEventListener('click', (e) => {
        switch (e.target.textContent) {
            case 'C':
                pressClearButton();
                break;
            case '=':
                pressEqualsButton();
                break;
            case '.':
                pressDecimalButton(e);
                break;
            case '\u21D0':
                pressBackspaceButton();
                break;
            default:
                pressOperatorButton(e);
        }
    }));
}

function pressClearButton() {
    clearCalculator();
}

function pressEqualsButton() {
    if (operands.length > operators.length) {
        calculateSum();
    }
}

function pressDecimalButton(e) {
    let target;
    try {
        target = e.target.textContent;
    } catch (err) {
        target = e.textContent;
    }
    if (!currentNumber.includes('.')) {
        currentNumber += '.';
        updateDisplay(target);
    }
}

function pressBackspaceButton() {
    let tempString, substring;
    tempString = document.getElementById("calculator-screen-previous").textContent;
    substring = tempString.substring(0, tempString.length - 1);
    if (operands.length > operators.length) {
        let tempNumber = currentNumber.substring(0, currentNumber.length - 1);
        clearDisplay();
        currentNumber = tempNumber;
        operands.pop();
        if (currentNumber != '') {
            operands.push(currentNumber);
        }
    } else {
        operators.pop();
        clearDisplay();
        currentNumber = operands.pop();
        operands.push(currentNumber);
    }
    updateDisplay(substring);
}

function pressOperatorButton(e) {
    let target;
    try {
        target = e.target.textContent;
    } catch (err) {
        target = e.textContent;
    }
    if (operators.length < operands.length) {
        updateDisplay(target);
        operators.push(target);
    }
}

function calculateSum() {
    clearDisplay();
    while (operands.length > 1) {
        let sum = operate(operators.pop(), operands.pop(), operands.pop());
        currentNumber = "" + sum;
        operands.push("" + sum);
    }
    updateDisplay("" + operands[0]);
}

function clearCalculator() {
    operands = [];
    operators = [];
    clearDisplay();
    displayString = "";
    currentNumber = "";
    currentDisplay = "";
}

function handleKeyboardPress(e) {
    let keyCode = e.keyCode;
    let buttonsNumpad = document.querySelector(`button[data-numpadkey="${keyCode}"]`);
    let buttons = buttonsNumpad;
    if (!buttonsNumpad) {
        buttons = document.querySelector(`button[data-key="${keyCode}"]`);
        if (!buttons) {
            return;
        }
    }
    //for number keys in keypad and keyboard
    if ((keyCode >= 96 && keyCode <= 105) || (keyCode >= 48 && keyCode <= 57)) {
        pressNumberButton(buttons);
        return;
    }

    //for keys add, subtract, multiply, divide
    if (keyCode == 107 || keyCode == 109 || keyCode == 106 || keyCode == 111 || keyCode == 191) {
        pressOperatorButton(buttons);
        return;
    }
    //for equals key
    if (keyCode == 13 || keyCode == 187) {
        e.preventDefault();
        pressEqualsButton();
        return;
    }
    //for decimal key
    if (keyCode == 110 || keyCode == 190) {
        pressDecimalButton(buttons);
        return;
    }
    //for backspace
    if (keyCode == 8) {
        pressBackspaceButton();
        return;
    }
    //for clear
    if (keyCode == 27) {
        pressClearButton();
        return;
    }
}

function setCurrentDisplay(string){
    document.getElementById("calculator-screen-current").textContent = string;
}

init();