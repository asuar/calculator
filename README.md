# calculator

Created with Javascript as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/web-development-101/lessons/calculator). 

# Final Thoughts

Completing this project allowed me to review javascript DOM manipulation, Eventlisteners, CSS grid and CSS Flexbox. I also learned about using color palettes in the design of websites.